import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';

@Component({
  selector: 'wooo-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {
  @Input() title: string;
  constructor(private router: Router, private store: Store) { }

  ngOnInit() {
  }
  goHome() {
    this.store.dispatch(new Navigate(['/']));
  }
}
