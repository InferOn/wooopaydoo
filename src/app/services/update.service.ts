import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { SnackbarComponent } from '../paydoo/shared/snackbar/snackbar.component';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {

  constructor(private swUpdate: SwUpdate, private snack: MatSnackBar) {
    this.swUpdate.available.subscribe(() => {
      const bar = this.snack.openFromComponent(SnackbarComponent, {
        duration: 1000,
        data: { msg: '...update available' },
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
        panelClass: 'snackbar-success',
      } as MatSnackBarConfig);

      bar.onAction().subscribe(() => {
        window.location.reload();
      });

      setTimeout(() => {
        snack.dismiss();
      }, 6000);
    });
  }
}
