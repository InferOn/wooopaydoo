import { TestBed } from '@angular/core/testing';

import { WoowService } from './woow.service';

describe('WoowService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WoowService = TestBed.get(WoowService);
    expect(service).toBeTruthy();
  });
});
