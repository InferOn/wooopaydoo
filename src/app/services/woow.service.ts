import { Injectable } from '@angular/core';
import { IMemberVM } from '../state/models';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { UUID } from 'angular2-uuid';
import { flatMap, map, tap, switchMap } from 'rxjs/operators';
import * as models from '../state/models';

@Injectable({
  providedIn: 'root'
})
export class WoowService {

  constructor(private localStorage: LocalStorage) { }

  destroyDb() {
    return this.localStorage.getItem<models.IPayDooState>('wooo-db')
      .pipe(
        flatMap(x => {
          const db = {
            members: [],
          } as models.IPayDooState;
          return this.localStorage.setItem('wooo-db', db);
        }),
      );
  }

  restoreDb() {
    return this.localStorage.getItem<models.IPayDooState>('wooo-db')
      .pipe(
        flatMap(x => {
          const db = {
            members: [],
          } as models.IPayDooState;
          db.members.push({
            name: 'Pio',
            id: UUID.UUID()
          } as IMemberVM);
          db.members.push({
            name: 'Amedeo',
            id: UUID.UUID()
          } as IMemberVM);
          return this.localStorage.setItem('wooo-db', db);
        }),
      );
  }

  updateMember(payload: models.IMemberVM) {
    return this.localStorage.getItem<models.IPayDooState>('wooo-db')
      .pipe(
        tap(x => {
          const oldMembers = x.members || [];
          const oldMembersWithoutTarget = oldMembers.filter(m => m.id !== payload.id);

          const freshMembers = [
            ...oldMembersWithoutTarget,
            {
              ...payload,
              choosed: false,
              checked: false
            } as IMemberVM
          ];
          return this.localStorage.setItem('wooo-db', {
            ...x,
            members: freshMembers
          } as models.IPayDooState).subscribe(y => {
            return this.localStorage.getItem<models.IPayDooState>('wooo-db')
              .subscribe((z) => {
                return z.members;
              });
          });
        })
      );

  }

  removeMember(payload: any) {
    return this.localStorage.getItem<models.IPayDooState>('wooo-db')
      .pipe(
        tap(x => {
          const membersWithoutTarget = x.members.filter((current) => {
            return current.id !== payload.member.id;
          });
          return this.localStorage.setItem('wooo-db', {
            members: membersWithoutTarget
          } as models.IPayDooState).subscribe(y => {
            return this.localStorage.getItem<models.IPayDooState>('wooo-db')
              .subscribe((z) => {
                return z.members;
              });
          });
        }),
      );
  }

  addMember(member: IMemberVM) {
    const tMember = {
      ...member,
      id: UUID.UUID(),
      memberFrom: new Date()
    } as IMemberVM;
    return this.localStorage.getItem<models.IPayDooState>('wooo-db')
      .pipe(
        flatMap(x => {
          const db = {
            members: x && x.members || [],
          } as models.IPayDooState;
          // db.members = x && x.members || [];
          db.members.push(tMember);
          return this.localStorage.setItem('wooo-db', db);
        }),
      );
  }

  getMembers() {
    return this.localStorage.getItem<models.IPayDooState>('wooo-db')
      .pipe(
        map(x => {
          return x && x.members ? x.members : [];
        })
      );
  }

  getPayments() {
    return this.localStorage.getItem<models.IPayDooState>('wooo-db')
      .pipe(
        map(x => {
          const payments = [];
          x.members.forEach(m => {
            payments.push(m.paymensts);
          });
          return payments;
        })
      );
  }
}


