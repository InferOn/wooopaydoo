import * as models from './models';

export class GetMembersAction {
  static readonly type = '[Wooo::get members]';
  constructor() {
  }
}

// export class SetSelected {
//   static readonly type = '[Wooo::set selected]';
//   constructor(public payload: models.IMember) { }
// }

// export class ToggleAllMemberToSelectionAction {
//   static readonly type = '[Wooo::toggle all members to selection]';
//   constructor(public $event: any) { }
// }

// export class ToggleMemberToSelectionAction {
//   static readonly type = '[Wooo::toggle member to selection]';
//   constructor(public $event: any, public payload: models.IMember) { }
// }
