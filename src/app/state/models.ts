
export interface IWoooState {
  justHere: boolean;
}

export interface IPayDooState extends IWoooState {
  members: IMemberVM[];
  memberForm: {
    model: undefined,
    dirty: false,
    status: '',
    errors: {}
  };
  payments: IPaymentVM[];
  currentPayment: IPaymentVM;
  isBreakView: boolean;
}

export interface IMember {
  id: string;
  name: string;
  from: Date;
  picture: string;

}

export interface IPaymentVM {
  amount: number;
  userId: string;
  name: string;
  date: Date;
  participants: IMemberVM[];
  purchases: IPurchase[];
}

export interface IMemberVM extends IMember {
  selected: boolean;
  checked: boolean;
  choosed: boolean;
  purchases: IPurchase[];
  paymensts: IPaymentVM[];
}

export interface IPurchase {
  cost: number;
  memberId: string;
}
export class Coffee implements IPurchase {
  memberId: string;
  cost = 1.10;
}

export class Juice implements IPurchase {
  memberId: string;
  cost = 3.00;
}

export class Cappuccino implements IPurchase {
  memberId: string;
  cost = 1.50;
}

export class Brioche implements IPurchase {
  memberId: string;
  cost = 1.20;
}

