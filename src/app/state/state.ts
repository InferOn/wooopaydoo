import { State, Selector } from '@ngxs/store';
import { StoreOptions } from '@ngxs/store/src/symbols';

import * as models from './models';

@State<models.IWoooState>({
  name: 'wooodb',
  defaults: {

  }
} as StoreOptions<models.IWoooState>)
export class WoooState {
  constructor() { }

}
