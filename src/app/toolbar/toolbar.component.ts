import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { RestoreDataAction, DestroyDataAction, GetMembersAction } from '../paydoo/state/actions';
import { environment } from '../../environments/environment';

@Component({
  selector: 'wooo-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  @Input() title: string | '';
  isDevelop: boolean;
  constructor(private store: Store) { }

  ngOnInit() {
    this.isDevelop = !environment.production;
  }
  restore() {
    this.store.dispatch([new RestoreDataAction(), new GetMembersAction()]);
  }
  destroy() {
    this.store.dispatch([new DestroyDataAction(), new GetMembersAction()]);
  }
}
