import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GenericValidator } from './GenericValidator';

import * as models from '../../state/models';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { ChangePortraitAction } from '../state/actions';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

import { UpdateFormDirty } from '@ngxs/form-plugin';
import { SnackbarComponent } from '../shared/snackbar/snackbar.component';

@Component({
  selector: 'wooo-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.scss'],
})
export class MemberDetailComponent implements OnInit, AfterViewInit {
  gender = 'male';

  @ViewChild('imgPortrait') imgPortrait: ElementRef;
  @ViewChild('memberName') memberName: any;
  @Output() saveEmitter: EventEmitter<models.IMemberVM> = new EventEmitter();
  portrait = this.randomPortrait(false);
  currentGenderFlag: boolean;

  memberForm: FormGroup;
  displayMessage: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } };
  private genericValidator: GenericValidator;
  currentGender: any;

  constructor(private fb: FormBuilder, private store: Store, public snackBar: MatSnackBar) {
    this.validationMessages = {
      name: {
        required: 'Name is required.',
        minlength: 'Name must be at least three characters.',
        maxlength: 'Name cannot exceed 20 characters.'
      },
    };
    this.genericValidator = new GenericValidator(this.validationMessages);
  }

  ngOnInit() {
    this.memberForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      female: [false, []],
      picture: ['']
    });
    this.memberForm.valueChanges.subscribe(
      value => {
        this.displayMessage = this.genericValidator.processMessages(this.memberForm);
      }
    );
  }
  ngAfterViewInit(): void {
  }

  change($event) {
    this.portrait = this.randomPortrait(($event as any).checked);
  }
  private randomPortrait(isALady: boolean) {
    this.currentGenderFlag = isALady;
    const rnd = Math.floor(Math.random() * (isALady === true ? 114 : 129 - 1 + 1)) + 1;
    return `assets/images/${isALady ? 'female' : 'male'}/${rnd}.png`;
  }
  changePortrait() {
    this.store.dispatch(new ChangePortraitAction()).subscribe(x => {
      this.portrait = this.randomPortrait(this.currentGenderFlag);
    });
  }
  onSwipeRight($event) {
    this.store.dispatch(new ChangePortraitAction()).subscribe(x => {
      this.gender = this.currentGenderFlag === true ? 'male' : 'female';
      this.portrait = this.randomPortrait(!this.currentGenderFlag);
    });
  }
  onSwipeLeft($event) {
    this.store.dispatch(new ChangePortraitAction()).subscribe(x => {
      this.gender = this.currentGenderFlag === true ? 'male' : 'female';
      this.portrait = this.randomPortrait(!this.currentGenderFlag);
    });
  }
  blur() {
  }
  saveDetail() {
    if (this.memberForm.valid && this.memberForm.dirty) {
      const member = this.memberForm.value;
      const b64 = this.baseImage(this.imgPortrait.nativeElement);
      (member as models.IMemberVM).picture = b64;
      this.saveEmitter.emit(member);
    }
  }
  finish() {
    this.reset();
  }
  private reset() {
    this.store.dispatch(
      new UpdateFormDirty({
        dirty: false,
        path: 'paydoodb.memberForm'
      })
    ).subscribe(() => {

      this.snackBar.openFromComponent(SnackbarComponent, {
        duration: 1000,
        data: { msg: 'successfully saved!' },
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
        panelClass: 'snackbar-success'
      } as MatSnackBarConfig);
      this.store.dispatch(new Navigate(['/']));
    });
  }
  private baseImage(img) {
    const canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0, img.width, img.height);
    return canvas.toDataURL('image/png');
  }
}


