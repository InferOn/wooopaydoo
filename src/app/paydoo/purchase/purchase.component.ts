import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { PayDooState } from '../state/state';
import { Coffee, Cappuccino, Brioche, Juice } from 'src/app/state/models';

@Component({
  selector: 'wooo-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.scss']
})
export class PurchaseComponent implements OnInit {
  @Output() purchaseCoffee: EventEmitter<void> = new EventEmitter();
  @Output() purchaseCappuccino: EventEmitter<void> = new EventEmitter();
  @Output() purchaseBrioche: EventEmitter<void> = new EventEmitter();
  @Output() purchaseJuice: EventEmitter<void> = new EventEmitter();

  @Output() pay: EventEmitter<void> = new EventEmitter();
  @Input() amount: number | 0;

  purchasedCoffee = 0;
  purchasedCappuccino = 0;
  purchasedBrioche = 0;
  purchasedJuice = 0;
  constructor(private store: Store) { }

  ngOnInit() {
    this.store.select(PayDooState.currentPayment$).subscribe((payment) => {
      const purchases = payment.purchases || [];
      this.purchasedCoffee = purchases.filter(x => {
        return x.cost === new Coffee().cost;
      }).length;
      this.purchasedCappuccino = purchases.filter(x => {
        return x.cost === new Cappuccino().cost;
      }).length;
      this.purchasedBrioche = purchases.filter(x => {
        return x.cost === new Brioche().cost;
      }).length;
      this.purchasedJuice = purchases.filter(x => {
        return x.cost === new Juice().cost;
      }).length;
    });
  }

  addCoffee() {
    this.purchaseCoffee.emit();
  }
  addCappuccino() {
    this.purchaseCappuccino.emit();
  }
  addBrioche() {
    this.purchaseBrioche.emit();
  }
  addJuice() {
    this.purchaseJuice.emit();

  }
  addPayment() {
    this.pay.emit();
  }
}
