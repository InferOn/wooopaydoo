import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaydooRoutingModule } from './paydoo-routing.module';
import { IndexComponent } from './index/index.component';
import { HomeComponent } from './home/home.component';
import { MemberDetailComponent } from './member-detail/member-detail.component';
import { CreateMemberComponent } from './create-member/create-member.component';
import { BreakComponent } from './break/break.component';
import { StatsComponent } from './stats/stats.component';
import { MembersComponent } from 'src/app/paydoo/members/members.component';
import { SnackbarComponent } from './shared/snackbar/snackbar.component';
import { PurchaseComponent } from './purchase/purchase.component';


import {
  MatAutocompleteModule, MatButtonModule, MatButtonToggleModule, MatCardModule,
  MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatExpansionModule,
  MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule,
  MatNativeDateModule, MatPaginatorModule, MatProgressBarModule,
  MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule,
  MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule,
  MatSortModule, MatTableModule, MatTabsModule, MatToolbarModule,
  MatTooltipModule, MatStepperModule, MatBadgeModule
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
library.add(fas, far);

import { NgxsModule } from '@ngxs/store';
import { NgxsConfig } from '@ngxs/store/src/symbols';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import * as more from 'highcharts/highcharts-more.src';
import * as exporting from 'highcharts/modules/exporting.src';
import { WebcamModule } from 'ngx-webcam';

import { PayDooState } from './state/state';

@NgModule({
  imports: [
    CommonModule,
    PaydooRoutingModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule,
    MatInputModule,
    MatBadgeModule,
    FontAwesomeModule,
    FormsModule, ReactiveFormsModule,
    NgxsModule.forFeature([PayDooState]),
    NgxsFormPluginModule,
    // NgxsLoggerPluginModule
    ChartModule,
    WebcamModule
  ],
  entryComponents: [SnackbarComponent],
  declarations: [IndexComponent, HomeComponent,
    MembersComponent, MemberDetailComponent, CreateMemberComponent,
    BreakComponent, StatsComponent, SnackbarComponent,
    PurchaseComponent],
  providers: [{ provide: HIGHCHARTS_MODULES, useFactory: () => [more, exporting] }]
})
export class PaydooModule { }
