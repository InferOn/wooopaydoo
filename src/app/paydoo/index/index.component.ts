import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';

import { fadeInAnimation } from '../../_animations/fade-in.animation';
import { IMemberVM } from 'src/app/state/models';
import { Store } from '@ngxs/store';
import {
  GetMembersAction, SwipeMemberCancelAction,
  CheckedMemberAction, ClickMemberWipeOutAction, SwipeMemberAction,
  UnCheckedMemberAction, ResetPaymentAction
} from 'src/app/paydoo/state/actions';
import { PayDooState } from '../state/state';
import { MembersComponent } from '../members/members.component';

@Component({
  selector: 'wooo-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  animations: [fadeInAnimation],
  // tslint:disable-next-line:use-host-property-decorator
  host: { '[@fadeInAnimation]': '' }
})
export class IndexComponent implements OnInit, AfterViewInit {
  @ViewChild(MembersComponent) membersComponent: MembersComponent;
  members: IMemberVM[];

  constructor(private store: Store) {
    this.store.select(PayDooState.members$).subscribe((members) => {
      this.members = members;
    });
  }

  ngOnInit() {
    this.store.dispatch([new ResetPaymentAction(), new GetMembersAction()]);
  }
  ngAfterViewInit(): void {
    this.membersComponent.Tap.subscribe((member: IMemberVM) => {
      if (member.checked === true) {
        this.store.dispatch(new SwipeMemberCancelAction(member));
      } else {
        this.store.dispatch(new CheckedMemberAction(member));
      }
    });

    this.membersComponent.SwipeLeft.subscribe((member: IMemberVM) => {
      if (member.checked === true) {
        this.store.dispatch(new ClickMemberWipeOutAction(member));
      } else {
        this.store.dispatch(new SwipeMemberAction(member));
      }
      if (member.checked === true) {
        this.store.dispatch(new UnCheckedMemberAction(member));
      }
    });

  }
}
