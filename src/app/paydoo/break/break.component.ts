import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { fadeInAnimation } from '../../_animations/fade-in.animation';
import { IMemberVM, IPaymentVM } from 'src/app/state/models';
import { PayDooState } from '../state/state';
import { Store } from '@ngxs/store';
import { MembersComponent } from '../members/members.component';
import {
  Navigate
} from '@ngxs/router-plugin';
import {
  ChooseMemberAction, UnChooseAllMemberAction, PurchaseCoffeeAction,
  PurchaseCappuccinoAction, PurchaseBriocheAction, PayAction,
  InitPaymentAction, PurchaseJuiceAction
} from '../state/actions';
import { PurchaseComponent } from '../purchase/purchase.component';
import { Observable, Subject } from 'rxjs';
import { WebcamInitError, WebcamImage, WebcamUtil } from 'ngx-webcam';

@Component({
  selector: 'wooo-break',
  templateUrl: './break.component.html',
  styleUrls: ['./break.component.scss'],
  animations: [fadeInAnimation],
  // tslint:disable-next-line:use-host-property-decorator
  host: { '[@fadeInAnimation]': '' }
})
export class BreakComponent implements OnInit, AfterViewInit {
  @ViewChild(MembersComponent) membersComponent: MembersComponent;
  @ViewChild(PurchaseComponent) purchaseComponent: PurchaseComponent;

  private trigger: Subject<void> = new Subject<void>();
  private nextWebcam: Subject<boolean | string> = new Subject<boolean | string>();
  public allowCameraSwitch = true;
  public deviceId: string;
  public showWebcam = false;
  public multipleWebcamsAvailable = false;
  public videoOptions: MediaTrackConstraints = {
    width: { ideal: 1024 },
    height: { ideal: 576 }
  };
  public errors: WebcamInitError[] = [];

  public webcamImage: WebcamImage = null;


  checkedMembers: IMemberVM[];
  choosedMember: IMemberVM;
  currentPayment: IPaymentVM;
  constructor(private store: Store) { }

  ngOnInit() {
    WebcamUtil.getAvailableVideoInputs()
      .then((mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
      });

    this.store.select(PayDooState.selectedMembers$).subscribe((members) => {
      this.checkedMembers = members;
    });

    this.store.select(PayDooState.choosedMember$).subscribe((member) => {
      this.choosedMember = member;
    });

    this.store.select(PayDooState.currentPayment$).subscribe((payment) => {
      this.currentPayment = payment;
    });
    this.store.dispatch(new InitPaymentAction());
  }

  ngAfterViewInit(): void {

    this.membersComponent.Tap.subscribe((member: IMemberVM) => {
      this.store.dispatch([new UnChooseAllMemberAction(), new ChooseMemberAction(member)]);
    });
    this.purchaseComponent.purchaseCoffee.subscribe(() => {
      this.store.dispatch(new PurchaseCoffeeAction());
    });
    this.purchaseComponent.purchaseCappuccino.subscribe(() => {
      this.store.dispatch(new PurchaseCappuccinoAction());
    });
    this.purchaseComponent.purchaseBrioche.subscribe(() => {
      this.store.dispatch(new PurchaseBriocheAction());
    });
    this.purchaseComponent.purchaseJuice.subscribe(() => {
      this.store.dispatch(new PurchaseJuiceAction());
    });


    this.purchaseComponent.pay.subscribe(() => {
      this.store.dispatch(new PayAction()).subscribe(x => {
        this.store.dispatch([new Navigate(['/'])]);
      });
    });
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }
  public get nextWebcamObservable(): Observable<boolean | string> {
    return this.nextWebcam.asObservable();
  }
  public cameraWasSwitched(deviceId: string): void {
    console.log('active device: ' + deviceId);
    this.deviceId = deviceId;
  }

  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
  }

  public triggerSnapshot(): void {
    this.trigger.subscribe((v) => {
    });
    this.trigger.next();
  }

  public handleImage(webcamImage: WebcamImage): void {
    // tslint:disable-next-line:no-console
    console.info('received webcam image', webcamImage);
    this.webcamImage = webcamImage;
    const imageAsBase64 = webcamImage.imageAsBase64;
    const imageAsDataUrl = webcamImage.imageAsDataUrl;

    this.showWebcam = false;

    this.store.dispatch([new Navigate(['/'])]);

  }

}
