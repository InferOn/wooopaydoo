import { IMemberVM } from 'src/app/state/models';


export class UndoAction {
  static readonly type = '[Paydoo:: Undo Action]';
  constructor() {
  }
}

export class DestroyDataAction {
  static readonly type = '[Paydoo:: Destroy Data Action]';
  constructor() {
  }
}
export class RestoreDataAction {
  static readonly type = '[Paydoo:: Restore Data Action]';
  constructor() {
  }
}

export class CheckedMemberAction {
  static readonly type = '[Paydoo:: Checked Member Action]';
  constructor(public member: IMemberVM) {
  }
}

export class UnCheckedMemberAction {
  static readonly type = '[Paydoo:: UnChecked Member Action]';
  constructor(public member: IMemberVM) {
  }
}

export class SwipeMemberAction {
  static readonly type = '[Paydoo:: Swipe Member Action]';
  constructor(public member: IMemberVM) {
  }
}
export class SwipeMemberCancelAction {
  static readonly type = '[Paydoo:: Swipe Member Cancel Action]';
  constructor(public member: IMemberVM) {
  }
}

export class ChooseMemberAction {
  static readonly type = '[Paydoo:: Choose Member Action]';
  constructor(public member: IMemberVM) {
  }
}

export class InitPaymentAction {
  static readonly type = '[Paydoo:: Init Payment Action]';
  constructor() {
  }
}
export class PurchaseCoffeeAction {
  static readonly type = '[Paydoo:: Purchase Coffee Action]';
  constructor() {
  }
}

export class SelectMemberWarningAction {
  static readonly type = '[Paydoo:: Select Member Warning Action]';
  constructor() {
  }
}

export class PurchaseCappuccinoAction {
  static readonly type = '[Paydoo:: Purchase Cappuccino Action]';
  constructor() {
  }
}

export class PurchaseBriocheAction {
  static readonly type = '[Paydoo:: Purchase Brioche Action]';
  constructor() {
  }
}

export class PurchaseJuiceAction {
  static readonly type = '[Paydoo:: Purchase Juice Action]';
  constructor() {
  }
}

export class PayAction {
  static readonly type = '[Paydoo:: Pay Action]';
  constructor() {
  }
}
export class UnChooseMemberAction {
  static readonly type = '[Paydoo:: UnChoose Member Action]';
  constructor(public member: IMemberVM) {
  }
}

export class UnChooseAllMemberAction {
  static readonly type = '[Paydoo:: Unchoose All Member Action]';
  constructor() {
  }
}

export class ChangePortraitAction {
  static readonly type = '[Paydoo:: Change Portrait Action]';
  constructor() {
  }
}

export class ClickMemberWipeOutAction {
  static readonly type = '[Paydoo:: Click Member WipeOut Action]';
  constructor(public member: IMemberVM) {
  }
}

export class AddMemberAction {
  static readonly type = '[Paydoo:: add member]';
  constructor(public payload: IMemberVM) {
  }
}

export class GetMembersAction {
  static readonly type = '[Paydoo:: get members]';
  constructor() {
  }
}

export class ResetPaymentAction {
  static readonly type = '[Paydoo:: Reset Payment Action]';
  constructor() {
  }
}

export class GetPaymentsAction {
  static readonly type = '[Paydoo:: Get Payments Action]';
  constructor() {
  }
}


