import { State, Selector, Action } from '@ngxs/store';
import { StoreOptions, StateContext, ActionOptions } from '@ngxs/store/src/symbols';

import * as models from '../../state/models';
import {
  AddMemberAction, GetMembersAction, SwipeMemberAction, SwipeMemberCancelAction, ClickMemberWipeOutAction,
  ChangePortraitAction,
  CheckedMemberAction,
  UnCheckedMemberAction,
  ChooseMemberAction,
  UnChooseAllMemberAction,
  UnChooseMemberAction,
  PurchaseCoffeeAction,
  PurchaseCappuccinoAction,
  PurchaseBriocheAction,
  PayAction,
  ResetPaymentAction,
  GetPaymentsAction,
  RestoreDataAction,
  DestroyDataAction,
  InitPaymentAction,
  UndoAction,
  PurchaseJuiceAction
} from './actions';
import { WoowService } from 'src/app/services/woow.service';
import { tap } from 'rxjs/operators';
import { Utils } from './Utils';

@State<models.IPayDooState>({
  name: 'paydoodb',
  defaults: {
    members: [] as models.IMemberVM[],
    memberForm: {
      model: undefined,
      dirty: false,
      status: '',
      errors: {}
    },
    isBreakView: false,
    currentPayment: { userId: null, amount: 0 } as models.IPaymentVM,
  }
} as StoreOptions<models.IPayDooState>)

export class PayDooState {

  constructor(private service: WoowService) {

  }

  @Selector()
  static payments$(state: models.IPayDooState) {
    const payments = [];
    state.members.forEach(m => {
      if (m.paymensts) {
        m.paymensts.forEach(p => {
          payments.push(p);
        });
      }
    });
    return payments;
  }

  @Selector()
  static currentPayment$(state: models.IPayDooState) {
    return state.currentPayment;
  }
  @Selector()
  static currentAmount$(state: models.IPayDooState) {
    return state.currentPayment.amount;
  }
  @Selector()
  static choosedMember$(state: models.IPayDooState) {
    return state.members.find((member) => member.choosed === true);
  }
  @Selector()
  static selectedMembers$(state: models.IPayDooState) {
    return state.members.filter((member) => member.checked === true);
  }
  @Selector()
  static members$(state: models.IPayDooState) {
    return state.members;
  }

  @Action([UndoAction], { cancelUncompleted: false } as ActionOptions)
  undoAction({ getState, setState }: StateContext<models.IPayDooState>, { }: models.IPayDooState) {
    const staleState = getState();
    const staleParticipants = staleState.currentPayment.participants;
    const staleAmount = staleState.currentPayment.amount;
    const stalePurchases = staleState.currentPayment.purchases;

    const freshPurchases = [...stalePurchases] as models.IPurchase[];
    const undoedPurchase = freshPurchases.pop();

    if (undoedPurchase) {
      const freshPaticipantsWithoutTarget = [...staleParticipants.filter(x => x.id !== undoedPurchase.memberId)];
      const freshParticipantTarget = { ...staleParticipants.find(x => x.id === undoedPurchase.memberId) };
      freshParticipantTarget.purchases.pop();

      setState({
        ...staleState,
        currentPayment: {
          ...staleState.currentPayment,
          amount: staleAmount - undoedPurchase.cost,
          participants: [...freshPaticipantsWithoutTarget, freshParticipantTarget],
          purchases: [...freshPurchases],
        } as models.IPaymentVM,
      } as models.IPayDooState);
    }
  }

  @Action([DestroyDataAction], { cancelUncompleted: false } as ActionOptions)
  destroyDataAction({ getState, setState }: StateContext<models.IPayDooState>, { }: models.IPayDooState) {
    return this.service.destroyDb().pipe(
      tap(x => {
        this.service.getMembers().subscribe((m) => {
          const state = getState();
          setState({
            ...state,
            members: []
          });
        });
      })
    );
  }

  @Action([RestoreDataAction], { cancelUncompleted: false } as ActionOptions)
  restoreDataAction({ getState, setState }: StateContext<models.IPayDooState>, { }: models.IPayDooState) {
    return this.service.restoreDb().pipe(
      tap(x => {
        this.service.getMembers().subscribe((m) => {
          const state = getState();
          setState({
            ...state,
            members: m
          });
        });
      })
    );
  }

  @Action([GetPaymentsAction], { cancelUncompleted: false } as ActionOptions)
  getPaymentsAction({ getState, setState }: StateContext<models.IPayDooState>, { }: models.IPayDooState) {
    return this.service.getPayments().pipe(tap(x => {
      const state = getState();
      const py = x.sort(Utils.sortPayments());
      const viewModels = [
        ...py
      ] as models.IPaymentVM[];
      setState({
        ...state,
        payments: viewModels
      });
    }));
  }

  @Action(ResetPaymentAction, { cancelUncompleted: false })
  resetPaymentAction({ getState, setState }: StateContext<models.IPayDooState>) {
    const oldState = getState();
    setState({
      ...oldState,
      currentPayment: { userId: null, amount: 0, participants: [] } as models.IPaymentVM
    });
  }

  @Action(InitPaymentAction, { cancelUncompleted: false })
  initPaymentAction({ getState, setState }: StateContext<models.IPayDooState>) {
    const oldState = getState();
    setState({
      ...oldState,
      currentPayment: { userId: null, amount: 0, participants: [] } as models.IPaymentVM
    });
  }

  @Action(PayAction, { cancelUncompleted: false })
  payAction({ getState, setState }: StateContext<models.IPayDooState>) {
    const oldState = getState();
    const memberToAddPayment = oldState.members.find(m => m.choosed === true);
    const freshMemberWithNewPayment = {
      ...memberToAddPayment,
      paymensts: [
        ...memberToAddPayment.paymensts || [],
        {
          ...oldState.currentPayment,
          date: new Date()
        } as models.IPaymentVM
      ]
    } as models.IMemberVM;

    return this.service.updateMember(freshMemberWithNewPayment).pipe(
      tap((success) => {
        if (success) {
          const freshState = {
            ...oldState,
          };
          setState(freshState);
        } else {
          throw new Error('...error persisting payment');
        }
      }),
    );
  }

  @Action(PurchaseJuiceAction, { cancelUncompleted: false })
  purchaseJuiceAction({ dispatch, patchState, getState, setState }: StateContext<models.IPayDooState>) {
    const { choosedMember, oldState, currentAmout, purchase, freshParticipants } = this.getPurchaseContext(getState, new models.Juice());
    if (choosedMember) {
      this.updatePurchase(setState, oldState, choosedMember, currentAmout, purchase, freshParticipants);
    }
  }


  @Action(PurchaseCoffeeAction, { cancelUncompleted: false })
  purchaseCoffeeAction({ dispatch, patchState, getState, setState }: StateContext<models.IPayDooState>) {
    const { choosedMember, oldState, currentAmout, purchase, freshParticipants } = this.getPurchaseContext(getState, new models.Coffee());
    if (choosedMember) {
      this.updatePurchase(setState, oldState, choosedMember, currentAmout, purchase, freshParticipants);
    }
  }

  @Action(PurchaseCappuccinoAction, { cancelUncompleted: false })
  purchaseCappuccinoAction({ getState, setState }: StateContext<models.IPayDooState>) {
    const { choosedMember, oldState, currentAmout, purchase, freshParticipants } =
      this.getPurchaseContext(getState, new models.Cappuccino());
    if (choosedMember) {
      this.updatePurchase(setState, oldState, choosedMember, currentAmout, purchase, freshParticipants);
    }
  }

  @Action(PurchaseBriocheAction, { cancelUncompleted: false })
  purchaseBriocheAction({ getState, setState }: StateContext<models.IPayDooState>) {
    const { choosedMember, oldState, currentAmout, purchase, freshParticipants } =
      this.getPurchaseContext(getState, new models.Brioche());
    if (choosedMember) {
      this.updatePurchase(setState, oldState, choosedMember, currentAmout, purchase, freshParticipants);
    }
  }

  @Action(UnChooseAllMemberAction, { cancelUncompleted: false })
  unchooseAllMemberAction({ getState, setState }: StateContext<models.IPayDooState>) {
    const oldState = getState();
    const freshMembers = [];
    oldState.members.map((m) => {
      freshMembers.push({
        ...m,
        choosed: false
      } as models.IMemberVM);
    });
    setState({
      ...oldState,
      members: freshMembers.sort(Utils.sortMembers())
    });
  }

  @Action(ChooseMemberAction, { cancelUncompleted: false })
  chooseMemberAction({ dispatch, patchState, getState, setState, }: StateContext<models.IPayDooState>,
    payload: ChooseMemberAction) {
    const target = payload.member as models.IMemberVM;
    const oldState = getState();
    const currentPayment = oldState.currentPayment || { userId: null, amount: 0, participants: [] } as models.IPaymentVM;
    const membersWithoutTarget = oldState.members.filter(mbr => mbr.id !== target.id);
    const freshMember = {
      ...target,
      choosed: true
    };
    const freshMembers = [
      ...membersWithoutTarget,
      freshMember
    ].sort(Utils.sortMembers());
    setState({
      ...oldState,
      currentPayment: {
        ...currentPayment,
        amount: currentPayment.amount || 0,
        userId: target.id,
        name: target.name
      } as models.IPaymentVM,
      members: freshMembers
    });
  }

  @Action(UnChooseMemberAction, { cancelUncompleted: false })
  unChooseMemberAction({ dispatch, patchState, getState, setState, }: StateContext<models.IPayDooState>,
    payload: UnChooseMemberAction) {
    const target = payload.member as models.IMemberVM;
    const oldState = getState();
    const membersWithoutTarget = oldState.members.filter(mbr => mbr.id !== target.id);
    const freshMember = {
      ...target,
      choosed: false
    };
    const freshMembers = [
      ...membersWithoutTarget,
      freshMember
    ].sort(Utils.sortMembers());
    setState({
      ...oldState,
      members: freshMembers
    });
  }

  @Action(ChangePortraitAction, { cancelUncompleted: false })
  changePortraitAction({ dispatch, patchState, getState, setState, }: StateContext<models.IPayDooState>, payload: any) { }

  @Action(UnCheckedMemberAction, { cancelUncompleted: false })
  unCheckedMemberAction({ dispatch, patchState, getState, setState, }: StateContext<models.IPayDooState>,
    payload: UnCheckedMemberAction) {
    const target = payload.member as models.IMemberVM;
    const oldState = getState();
    const membersWithoutTarget = oldState.members.filter(mbr => mbr.id !== target.id);
    const freshMember = {
      ...target,
      checked: false
    };
    const freshMembers = [
      ...membersWithoutTarget,
      freshMember
    ].sort(Utils.sortMembers());
    setState({
      ...oldState,
      members: freshMembers
    });
  }

  @Action(CheckedMemberAction, { cancelUncompleted: false })
  checkedMemberAction({ dispatch, patchState, getState, setState, }: StateContext<models.IPayDooState>,
    payload: CheckedMemberAction) {
    const target = payload.member as models.IMemberVM;
    const oldState = getState();
    const membersWithoutTarget = oldState.members.filter(mbr => mbr.id !== target.id);
    const freshMember = {
      ...target,
      checked: true
    };
    const freshMembers = [
      ...membersWithoutTarget,
      freshMember
    ].sort(Utils.sortMembers());
    setState({
      ...oldState,
      members: freshMembers
    });
  }

  @Action(SwipeMemberCancelAction, { cancelUncompleted: false })
  swipeMemberCancelAction({ dispatch, patchState, getState, setState, }: StateContext<models.IPayDooState>, payload: any) {
    const target = payload.member as models.IMemberVM;
    const oldState = getState();
    const membersWithoutTarget = oldState.members.filter(mbr => mbr.id !== target.id);
    const freshMember = {
      ...target,
      checked: false
    };
    const freshMembers = [
      ...membersWithoutTarget,
      freshMember
    ].sort(Utils.sortMembers());
    setState({
      ...oldState,
      members: freshMembers
    });
  }

  @Action(SwipeMemberAction, { cancelUncompleted: false })
  swipeMember({ dispatch, patchState, getState, setState, }: StateContext<models.IPayDooState>, payload: any) {
    const target = payload.member as models.IMemberVM;
    const oldState = getState();
    const membersWithoutTarget = oldState.members.filter(mbr => mbr.id !== target.id);
    const freshMember = {
      ...target,
      checked: true
    };
    const freshMembers = [
      ...membersWithoutTarget,
      freshMember
    ].sort(Utils.sortMembers());
    setState({
      ...oldState,
      members: freshMembers
    });
  }

  @Action(ClickMemberWipeOutAction, { cancelUncompleted: false })
  wipoutMember({ dispatch, getState, setState, }: StateContext<models.IPayDooState>, payload: ClickMemberWipeOutAction) {
    const target = payload.member as models.IMemberVM;
    const oldState = getState();
    const membersWithoutTarget = oldState.members.filter(mbr => mbr.id !== target.id);
    return this.service.removeMember(payload).subscribe(x => {
      setState({
        ...oldState,
        members: membersWithoutTarget
      });
    });
  }

  @Action(AddMemberAction, { cancelUncompleted: false })
  addMember({ dispatch, patchState, getState, setState, }: StateContext<models.IPayDooState>, { payload }: any) {
    return this.addMemberImp(getState, payload, setState);
  }

  private addMemberImp(getState: () => models.IPayDooState, payload: any, setState: (val: models.IPayDooState) => any) {
    const oldState = getState();
    const freshMembers = [...oldState.members, payload];
    return this.service.addMember(payload).pipe(tap(success => {
      if (success) {
        setState({
          ...oldState,
          members: freshMembers
        });
      } else {
        // TODO: manage errors => snackbar???
        console.log('service.addMember: error');
      }
    }));
  }

  @Action([GetMembersAction], {
    cancelUncompleted: false
  } as ActionOptions)
  getMembers({ getState, setState }: StateContext<models.IPayDooState>, { }: models.IPayDooState) {
    return this.getMembersImp(getState, setState);
  }

  private getMembersImp(getState: () => models.IPayDooState, setState: (val: models.IPayDooState) => any) {
    return this.service.getMembers().pipe(tap(x => {
      const state = getState();
      const mbrs = x.sort(Utils.sortMembers());
      const viewModels = [
        ...mbrs
      ] as models.IMemberVM[];
      setState({
        ...state,
        members: viewModels
      });
    }));
  }

  /* private */

  private getPurchaseContext(getState: () => models.IPayDooState, purchase: models.IPurchase) {
    const oldState = getState();
    const choosedMember = oldState.members.find((m) => m.choosed === true);
    const currentAmout = oldState.currentPayment.amount;
    const participants = oldState.currentPayment.participants && oldState.currentPayment.participants.length > 0 ?
      oldState.currentPayment.participants :
      oldState.members.filter(x => x.checked === true);

    const participantsWithoutChoosed = participants.filter(x => x.id !== choosedMember.id);
    const oldParticipant = participants.find(x => x.id === choosedMember.id) as models.IMemberVM;
    const freshPurchases = this.getFreshPurchases(oldParticipant, purchase);
    const freshParticipant = this.getFreshParticipant(oldParticipant, freshPurchases);
    const freshParticipants = this.getFreshParticipants(participantsWithoutChoosed, freshParticipant);
    return { choosedMember, oldState, currentAmout, purchase, freshParticipants };
  }

  private getFreshPurchases(oldParticipant: models.IMemberVM, purchase: models.Coffee) {
    const targetParticipant = oldParticipant || {} as models.IMemberVM;
    const freshParticipant = {
      ...targetParticipant,
      purchases: targetParticipant.purchases || [] as models.IPurchase[]
    };
    return [
      ...freshParticipant.purchases,
      purchase
    ];
  }

  private getFreshParticipants(participantsWithoutChoosed: models.IMemberVM[], freshParticipant: models.IMemberVM) {
    return [
      ...participantsWithoutChoosed,
      freshParticipant
    ];
  }

  private getFreshParticipant(oldParticipant: models.IMemberVM, freshPurchases: models.IPurchase[]) {
    const targetParticipant = oldParticipant || {
      purchases: []
    };
    return {
      ...targetParticipant,
      purchases: freshPurchases
    } as models.IMemberVM;
  }

  private updatePurchase(setState: (val: models.IPayDooState) => any,
    oldState: models.IPayDooState,
    choosedMember: models.IMemberVM,
    currentAmout: number, purchase: models.IPurchase, freshParticipants: models.IMemberVM[]) {
    const targetPurchase = {
      ...purchase,
      memberId: choosedMember.id
    } as models.IPurchase;

    setState({
      ...oldState,
      currentPayment: {
        ...oldState.currentPayment,
        userId: choosedMember.id,
        name: choosedMember.name,
        amount: currentAmout + purchase.cost,
        participants: freshParticipants,
        purchases: [
          ...oldState.currentPayment.purchases || [],
          targetPurchase
        ]
      } as models.IPaymentVM
    });
  }
}
