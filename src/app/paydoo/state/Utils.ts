import * as models from '../../state/models';
export class Utils {
  public static sortMembers() {
    return (a: models.IMemberVM, b: models.IMemberVM): number => {
      if (a.name.toLowerCase() < b.name.toLowerCase()) {
        return -1;
      }
      if (a.name.toLowerCase() > b.name.toLowerCase()) {
        return 1;
      }
      return 0;
    };
  }
  public static sortPayments() {
    return (a: models.IPaymentVM, b: models.IPaymentVM): number => {
      // if (a.name.toLowerCase() < b.name.toLowerCase()) {
      //   return -1;
      // }
      // if (a.name.toLowerCase() > b.name.toLowerCase()) {
      //   return 1;
      // }
      return 0;
    };
  }
  public baseImage(img) {
    const canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0, img.width, img.height);
    return canvas.toDataURL('image/png');
  }

}
