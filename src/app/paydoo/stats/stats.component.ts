import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from '../../_animations/fade-in.animation';
import { Chart } from 'angular-highcharts';
import { Store } from '@ngxs/store';
import { GetPaymentsAction } from '../state/actions';
import { PayDooState } from '../state/state';
import { IPaymentVM } from 'src/app/state/models';

@Component({
  selector: 'wooo-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss'],
  animations: [fadeInAnimation],
  // tslint:disable-next-line:use-host-property-decorator
  host: { '[@fadeInAnimation]': '' }
})
export class StatsComponent implements OnInit {
  chart: Chart;
  Payments: IPaymentVM[];
  labels = [];
  data = [];
  constructor(private store: Store) {
  }

  ngOnInit() {
    this.store.dispatch([
      new GetPaymentsAction()

    ]);

    this.store.select(PayDooState.payments$).subscribe((payments) => {
      const currentPayments = payments || [];
      const labels = [];
      const data = [];


      const groups = currentPayments.reduce((a, e) => {
        const estKey = (e['name']);
        (a[estKey] ? a[estKey] : (a[estKey] = null || [])).push(e);
        return a;
      }, {});
      for (const label in groups) {
        if (groups.hasOwnProperty(label)) {
          labels.push(label);
          const tdata = groups[label].map((g) => {
            return g.amount;
          }).reduce((aa, ee) => {
            return aa + ee;
          });

          data.push(parseFloat(tdata.toFixed(2)));
        }
      }
      this.createChart(labels, data);
    });

  }

  private createChart(labels, data) {
    this.chart = new Chart({
      chart: {
        type: 'bar'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      tooltip: {
        valueSuffix: ' euro'
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      series: [{
        name: '',
        data: data
      }],
      xAxis: {
        categories: labels,
        title: {
          text: null
        }
      },
      credits: {
        enabled: false
      },
    } as Highcharts.Options);
  }
}
