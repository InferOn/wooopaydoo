import { Component, OnInit, ViewChild } from '@angular/core';
import { fadeInAnimation } from '../../_animations/fade-in.animation';
import { MemberDetailComponent } from '../member-detail/member-detail.component';
import { Store } from '@ngxs/store';
import { AddMemberAction } from '../state/actions';

@Component({
  selector: 'wooo-create-member',
  templateUrl: './create-member.component.html',
  styleUrls: ['./create-member.component.scss'],
  animations: [fadeInAnimation],
  // tslint:disable-next-line:use-host-property-decorator
  host: { '[@fadeInAnimation]': '' }
})
export class CreateMemberComponent implements OnInit {
  @ViewChild(MemberDetailComponent) detail: MemberDetailComponent;
  constructor(private store: Store) { }

  ngOnInit() {
    this.detail.saveEmitter.subscribe((member => {
      this.store.dispatch(new AddMemberAction(member)).subscribe((result) => {
        this.detail.finish();
      });
    }));
  }
}
