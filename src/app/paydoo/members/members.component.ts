import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Store, Actions } from '@ngxs/store';
import {
  SwipeMemberCancelAction, ClickMemberWipeOutAction,
  CheckedMemberAction, UnCheckedMemberAction
} from '../state/actions';
import { IMemberVM, IPaymentVM, Coffee, Cappuccino, Brioche, Juice } from 'src/app/state/models';
import { PayDooState } from '../state/state';


@Component({
  selector: 'wooo-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {
  @Input() selectable: boolean;
  @Input() deletable: boolean;
  @Input() members;

  @Output() SwipeRight: EventEmitter<IMemberVM> = new EventEmitter();
  @Output() SwipeLeft: EventEmitter<IMemberVM> = new EventEmitter();
  @Output() Tap: EventEmitter<IMemberVM> = new EventEmitter();
  currentPayment: IPaymentVM;

  constructor(private store: Store, private actions$: Actions) {

  }

  ngOnInit() {
    this.store.select(PayDooState.currentPayment$).subscribe((payment) => {
      this.currentPayment = payment;
    });
  }

  memberCoffee(member): number {
    if (this.currentPayment) {
      const part = this.currentPayment.participants.find(m => m.id === member.id);
      if (part && part.purchases) {
        return part.purchases.filter(x => x instanceof Coffee).length;
      }
    }
    return 0;
  }

  memberCappuccino(member): number {
    if (this.currentPayment) {
      const part = this.currentPayment.participants.find(m => m.id === member.id);
      if (part && part.purchases) {
        return part.purchases.filter(x => x instanceof Cappuccino).length;
      }
    }
    return 0;
  }

  memberBrioche(member): number {
    if (this.currentPayment) {
      const part = this.currentPayment.participants.find(m => m.id === member.id);
      if (part && part.purchases) {
        return part.purchases.filter(x => x instanceof Brioche).length;
      }
    }
    return 0;
  }

  memberJuice(member): number {
    if (this.currentPayment) {
      const part = this.currentPayment.participants.find(m => m.id === member.id);
      if (part && part.purchases) {
        return part.purchases.filter(x => x instanceof Juice).length;
      }
    }
    return 0;
  }

  protected onSwipeRight($event, member) {
    this.SwipeRight.emit(member);
  }
  protected onSwipeLeft($event, member) {
    this.SwipeLeft.emit(member);
  }
  protected onTap($event, member) {
    this.Tap.emit(member);
  }
  onCancel($event, member) {
    this.store.dispatch(new SwipeMemberCancelAction(member));
  }
  onRemove($event, member) {
    this.store.dispatch(new ClickMemberWipeOutAction(member));
  }
  onUncheck($event, member) {
    this.store.dispatch(new UnCheckedMemberAction(member));
  }
  onCheckMember($event, member) {
    if ($event.checked) {
      this.store.dispatch(new CheckedMemberAction(member));
    } else {
      this.store.dispatch(new UnCheckedMemberAction(member));
    }
  }
}
