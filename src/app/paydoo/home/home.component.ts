import { Component, OnInit } from '@angular/core';
import { slideInOutAnimation } from '../../_animations/slide-in-out.animation';
import { fadeInAnimation } from '../../_animations/fade-in.animation';

@Component({
  selector: 'wooo-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [slideInOutAnimation],
  // tslint:disable-next-line:use-host-property-decorator
  host: { '[@slideInOutAnimation]': '' }
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
