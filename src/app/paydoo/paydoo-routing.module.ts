import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { HomeComponent } from 'src/app/paydoo/home/home.component';
import { CreateMemberComponent } from './create-member/create-member.component';
import { BreakComponent } from 'src/app/paydoo/break/break.component';
import { StatsComponent } from 'src/app/paydoo/stats/stats.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent, pathMatch: 'prefix',
    children: [
      { path: '', redirectTo: 'index', pathMatch: 'prefix' },
      { path: 'index', component: IndexComponent },
      { path: 'new', component: CreateMemberComponent },
      { path: 'break', component: BreakComponent },
      { path: 'stats', component: StatsComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaydooRoutingModule { }
