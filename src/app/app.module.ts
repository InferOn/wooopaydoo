
import {
  BrowserModule,
  HammerGestureConfig,
  HAMMER_GESTURE_CONFIG,
} from '@angular/platform-browser';

// declare var Hammer: HammerDefaults;
declare var Hammer: any;

export class AppHammerConfig extends HammerGestureConfig {
  buildHammer(element: HTMLElement) {
    const mc = new Hammer(element, {
      touchAction: 'pan-y'
    });
    return mc;
  }
}
import { localStorageProviders } from '@ngx-pwa/local-storage';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
library.add(fas, far);

import {
  MatToolbarModule, MatAutocompleteModule, MatButtonModule,
  MatButtonToggleModule, MatCardModule, MatCheckboxModule,
  MatChipsModule, MatDatepickerModule, MatDialogModule,
  MatExpansionModule, MatGridListModule, MatIconModule,
  MatInputModule, MatListModule, MatMenuModule,
  MatNativeDateModule, MatPaginatorModule,
  MatProgressBarModule, MatProgressSpinnerModule,
  MatRadioModule, MatRippleModule, MatSelectModule,
  MatSidenavModule, MatSliderModule, MatSlideToggleModule,
  MatSnackBarModule, MatSortModule, MatTableModule,
  MatTabsModule, MatTooltipModule, MatStepperModule,
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { DeviceDetectorModule } from 'ngx-device-detector';
import { ResponsiveModule } from 'ngx-responsive';

import { NgxsModule } from '@ngxs/store';
import { NgxsConfig } from '@ngxs/store/src/symbols';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';

import { WoooState } from './state/state';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { LogoComponent } from './logo/logo.component';
import { BottombarComponent } from './bottombar/bottombar.component';
import { PaydooModule } from './paydoo/paydoo.module';

import { AppRoutingModule } from './app-routing.module';
import { SnackbarComponent } from './snackbar/snackbar.component';
import { PayDooState } from './paydoo/state/state';
import { NgxsLoggerPluginOptions } from '@ngxs/logger-plugin/src/symbols';

const responsiveConfig = {
  breakPoints: {
    xs: { max: 600 },
    sm: { min: 601, max: 959 },
    md: { min: 960, max: 1279 },
    lg: { min: 1280, max: 1919 },
    xl: { min: 1920 }
  },
  debounceTime: 100
};

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    LogoComponent,
    BottombarComponent,
    SnackbarComponent,
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),

    BrowserAnimationsModule,
    FlexLayoutModule,
    FontAwesomeModule,

    DeviceDetectorModule.forRoot(),
    ResponsiveModule.forRoot(responsiveConfig),

    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule,

    FormsModule, ReactiveFormsModule,
    NgxsModule.forRoot([PayDooState], {
      developmentMode: false
    } as NgxsConfig),
    NgxsRouterPluginModule.forRoot(),
    NgxsFormPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot({
      disabled: environment.production
    } as NgxsLoggerPluginOptions),
    PaydooModule,
    AppRoutingModule,
    NgxsReduxDevtoolsPluginModule.forRoot()

  ],
  entryComponents: [SnackbarComponent],
  providers: [
    { provide: HAMMER_GESTURE_CONFIG, useClass: AppHammerConfig },
    localStorageProviders({ prefix: 'wooo-pay-dooo' })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
