import { Component, Input, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { UpdateService } from './services/update.service';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import {
  PurchaseCoffeeAction, PurchaseCappuccinoAction, DestroyDataAction, RestoreDataAction, CheckedMemberAction,
  UnCheckedMemberAction, SwipeMemberAction, SwipeMemberCancelAction, ChooseMemberAction, InitPaymentAction, SelectMemberWarningAction,
  PurchaseBriocheAction, PayAction, UnChooseMemberAction, UnChooseAllMemberAction, ChangePortraitAction,
  ClickMemberWipeOutAction, AddMemberAction, GetMembersAction, ResetPaymentAction, GetPaymentsAction
} from './paydoo/state/actions';
import { Navigate } from '@ngxs/router-plugin';

@Component({
  selector: 'wooo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  @Input() title = 'woouhuh';
  constructor(private updateService: UpdateService, private actions$: Actions, private store: Store) { }
  ngOnInit(): void {
    const awarsomeActions = [DestroyDataAction,
      RestoreDataAction,
      CheckedMemberAction,
      UnCheckedMemberAction,
      SwipeMemberAction,
      SwipeMemberCancelAction,
      ChooseMemberAction,
      InitPaymentAction,
      PurchaseCoffeeAction,
      SelectMemberWarningAction,
      PurchaseCappuccinoAction,
      PurchaseBriocheAction,
      PayAction,
      UnChooseMemberAction,
      UnChooseAllMemberAction,
      ChangePortraitAction,
      ClickMemberWipeOutAction,
      AddMemberAction,
      GetMembersAction,
      ResetPaymentAction,
      GetPaymentsAction,
      Navigate];
    awarsomeActions.forEach((act) => {
      this.actions$.pipe(ofActionDispatched(act)).subscribe(() => {

      });
    });
  }
}
