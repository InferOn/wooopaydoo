import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { PayDooState } from '../paydoo/state/state';
import { IMemberVM, IPaymentVM } from '../state/models';
import { environment } from '../../environments/environment';
import { UndoAction, PayAction } from '../paydoo/state/actions';
import { Navigate } from '@ngxs/router-plugin';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'wooo-bottombar',
  templateUrl: './bottombar.component.html',
  styleUrls: ['./bottombar.component.scss']
})
export class BottombarComponent implements OnInit {
  private currentUrl: string;
  checkedMembers: IMemberVM[];
  choosedMember: IMemberVM;
  appVersion: string;
  currentPayment: IPaymentVM;

  public get payEnabled(): boolean {
    return this.currentPayment && this.currentPayment.purchases && this.currentPayment.purchases.length > 0;
  }

  public get addUserVisible(): boolean {
    return this.currentUrl === '/index' || this.currentUrl === '/';
  }

  public get breakVisible(): boolean {
    return this.currentUrl === '/index' || this.currentUrl === '/';
  }

  public get goHomeVisible(): boolean {
    return this.currentUrl === '/break' || this.currentUrl === '/stats' || this.currentUrl === '/new';
  }

  public get undoVisible(): boolean {
    return this.currentUrl === '/break';
  }

  public get payVisible(): boolean {
    return this.currentUrl === '/break';
  }

  public get statsVisible(): boolean {
    return this.currentUrl === '/index' || this.currentUrl === '/';
  }

  constructor(private store: Store, private router: Router) {
    this.appVersion = `[ ${!environment.production ? 'develop:' : ''}${environment.version} ]`;
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.currentUrl = event.url;
      }
    });
    this.store.select(PayDooState.selectedMembers$).subscribe((members) => {
      this.checkedMembers = members;
    });
    this.store.select(PayDooState.choosedMember$).subscribe((member) => {
      this.choosedMember = member;
    });
    this.store.select(PayDooState.currentPayment$).subscribe((payment) => {
      this.currentPayment = payment;
    });
  }

  undo() {
    this.store.dispatch(new UndoAction());
  }
  addPayment() {
    this.store.dispatch(new PayAction()).subscribe(() => {
      this.store.dispatch([new Navigate(['/'])]);
    });
  }
}
