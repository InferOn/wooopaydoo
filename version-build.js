const { gitDescribeSync } = require('git-describe');
const { version } = require('../ngWoooPayDo/version');

const { resolve } = require('path');
const { writeFileSync } = require('fs-extra');

const gitInfo = gitDescribeSync({
  dirtyMark: false,
  dirtySemver: false
});

upgradeVersion(parseInt(process.argv[2].replace("--arg=", "")));

function upgradeVersion(v = 2) {
  const tVersion = version.split('.');
  let majorVersion = parseInt(tVersion[0]);
  let minorVersion = parseInt(tVersion[1]);
  let buildVersion = parseInt(tVersion[2]);

  switch (v) {
    case 0:
      majorVersion = parseInt(tVersion[0]) + 1;
      break;
    case 1:
      minorVersion = parseInt(tVersion[1]) + 1;
      break;
    case 2:
      buildVersion = parseInt(tVersion[2]) + 1;
      break;
    default:
      break;
  }
  const resolvedVersion = `${majorVersion}.${minorVersion}.${buildVersion}`;
  gitInfo.version = resolvedVersion;

  console.log("resolvedVersion: ", resolvedVersion);

  const file = resolve('src/version.ts');
  writeFileSync(file, `// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT OR CHECKIN!
/* tslint:disable */
export const VERSION = ${JSON.stringify(gitInfo, null, 4)};
/* tslint:enable */
`, { encoding: 'utf-8' });

  writeFileSync(resolve('version.json'), "{\"version\":\"" + resolvedVersion + "\"}", { encoding: 'utf-8' })
}

