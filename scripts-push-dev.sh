#set -o errexit # Exit on error
git checkout develop
npm run version-build
git add -A -- .
git commit --message="auto-commit"
git push
