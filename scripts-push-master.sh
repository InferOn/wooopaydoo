
#set -o errexit # Exit on error
git checkout master
git merge develop
npm run version-build
git add -A -- .
git commit --message="auto-commit"
git push origin master

